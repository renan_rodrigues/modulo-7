public class TestaConta {
    public static void main(String[] args){

            ContaCorrente cc1 = new ContaCorrente(22,1,"Banco AA",100,100);
            System.out.println(cc1);
            System.out.println(cc1.Sacar(80));
            System.out.println(cc1.getSaldo());
            System.out.println(cc1.Depositar(80));
            System.out.println(cc1.getSaldo());
            ContaSalario cc2 = new ContaSalario(22,1,"Banco AA", 5000);
            System.out.println(cc2);
            System.out.println(cc2.Sacar(500));
            System.out.println(cc2.getSaldo());
            System.out.println(cc2.Depositar(500));
            System.out.println(cc2.getSaldo());
            ContaPoupanca cc3 = new ContaPoupanca(22, 1, "Banco AA", 10000, 9, 1) ;
            System.out.println(cc3);
            System.out.println(cc3.getSaldo());
            System.out.println(cc3.Sacar(500));
            System.out.println(cc3.getSaldo());
            System.out.println(cc3.Depositar(500));
            System.out.println(cc3.getSaldo());
    }
}
