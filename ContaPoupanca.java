public class ContaPoupanca extends Conta {
    private int diaAniversario;
    private double taxadeJuros;
    private int valorSaque;
    private int valorDepositado;


    public ContaPoupanca(int numero, int agencia, String banco, double saldo, int diaAniversario, double taxaDeJuros) {
        super(numero, agencia, banco, saldo);
        this.diaAniversario = diaAniversario;
        this.taxadeJuros = taxaDeJuros;
    }

    @Override
    public String toString() {
        return "ContaPoupança{" +
                "Dia de Aniversário = " + diaAniversario + " ; " +
                "Taxa de Juros = " + taxadeJuros + '}';
    }

    @Override
    public String Sacar(int valorsaque) {

        if (diaAniversario < 10 && diaAniversario >= 1) {
            if (valorsaque <= this.saldo + (this.saldo * (taxadeJuros / 100 + 0.01))) {
                 saldo  = this.saldo-valorsaque;
            } else {
                System.out.print("Saldo Insuficiente :");
            }
        } else if (diaAniversario < 20 && diaAniversario >= 10) {
            if (valorsaque <= this.saldo + (this.saldo * (taxadeJuros / 100 + 0.02))) {
                saldo  = this.saldo-valorsaque;
            } else {
                System.out.print("Saldo Insuficiente :");
            }
        } else if (diaAniversario <= 31 && diaAniversario >= 20) {
            if (valorsaque <= this.saldo + (this.saldo * (taxadeJuros / 100 + 0.03))) {
                saldo  = this.saldo-valorsaque;
            } else {
                System.out.print("Saldo Insuficiente :");
            }
        }

        return "";
    }

    public String Depositar(int valordepositado) {
        saldo  = this.saldo+valordepositado;
        return "";
    }

    public double getSaldo() {
        if (diaAniversario < 10 && diaAniversario >= 1) {
            double Juros = (taxadeJuros / 100) + 0.01;
            saldo  = this.saldo + (this.saldo * Juros);
            return saldo;
        } else if (diaAniversario < 20 && diaAniversario >= 10) {
            double Juros = (taxadeJuros / 100) + 0.02;
            saldo  = this.saldo + (this.saldo * Juros);
            return saldo;
        } else if (diaAniversario <= 31 && diaAniversario >= 20) {
            double Juros = (taxadeJuros / 100) + 0.03;
            saldo  = this.saldo + (this.saldo * Juros);
            return saldo;
        } else {
            System.out.print("Não e possivel ver o Saldo :");
            return 0;
        }

    }

    public int getDiaAniversario() {
        return diaAniversario;
    }

    public void setDiaAniversario(int diaAniversario) {
        this.diaAniversario = diaAniversario;
    }

}