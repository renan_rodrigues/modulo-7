public class ContaCorrente extends Conta{
    private double chequeEspecial;
    private int valorSaque;
    private int valorDepositado;

    public ContaCorrente(int numero, int agencia, String banco, double saldo, double chequeEspecial) {
        super(numero, agencia, banco, saldo);
        this.chequeEspecial = chequeEspecial;

    }

         @Override
    public String toString() {
        return "ContaCorrente{" +
                "chequeEspecial=" + chequeEspecial +
                '}';
    }

    @Override
    public String Sacar(int valorsaque) {
        if(valorsaque> chequeEspecial + saldo){
            System.out.print("Saldo Insuficiente :");
            return "" ;
        }else
            saldo = this.saldo - valorsaque;
     //   System.out.println("1 :" + saldo);

        System.out.println("Valor do Saque :" + valorsaque);
        return "";
    }


    @Override
    public String Depositar(int valordepositado) {
        saldo = this.saldo + valordepositado;
        System.out.println("Valor do Deposito :" + valordepositado);
        return " ";
    }

    public double getSaldo(){
        saldo = this.saldo ;
        if(saldo<=0){
            chequeEspecial = this.chequeEspecial + saldo;
            saldo = 0;
            System.out.println("Cheque Especial: "+ chequeEspecial);
            System.out.println("Saldo Atual: "+ saldo);
        }else {
            System.out.println("Cheque Especial: "+ chequeEspecial);
            System.out.println("Saldo Atual: "+ saldo);
        }
    return this.chequeEspecial + saldo;


    }


}
